#!/usr/bin/python3
# coding: utf8

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

RED = 17
GREEN = 27
BLUE = 22

RGB_ON='0'
RGB_OFF='1'
RGB_ALL = '000'
RGB_NONE = '111'

GPIO.setup( RED,   GPIO.OUT)
GPIO.setup( GREEN, GPIO.OUT)
GPIO.setup( BLUE,  GPIO.OUT)

rgb_circle = [ '001', '011', '010', '110', '100', '101' ]
rgb_delay=0.125

def set_rgb(p_value):
  if (len(p_value) == 3):
    GPIO.output(RED,  int(p_value[0]))
    GPIO.output(GREEN,int(p_value[1]))
    GPIO.output(BLUE, int(p_value[2]))

set_rgb(RGB_NONE)

try:
  while (True):
    for item in rgb_circle:
      set_rgb(item)
      time.sleep(rgb_delay)
      set_rgb(RGB_NONE)
      time.sleep(rgb_delay * 4)

except KeyboardInterrupt:
  GPIO.cleanup()
